(if necessary) set TICK_DATABASE_URL (default http://localhost:3000/api/ticks):
$ nano ./config.txt

$ sudo apt-get install -y virtualenv

$ virtualenv -p python3 .env

If you see the following error:
locale.Error: unsupported locale setting, please run the following 3 commands:

$ export LC_ALL="en_US.UTF-8"

$ export LC_CTYPE="en_US.UTF-8"

$ sudo dpkg-reconfigure locales

Install python pip requirements:

$ .env/bin/pip install -r requirements.txt

Run program:

$ .env/bin/python run.py
