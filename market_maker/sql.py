import math

import sqlalchemy
from sqlalchemy.sql import text

from .evomabitmex.orders import Order, OrderBook
from .evomabitmex.strategy.base import AbstractStrategy
from .evomabitmex.utils import log

logger = log.setup_custom_logger(__name__)


def distribute(volume, n, fun):
    coeffs = list(map(fun, range(n)))
    coeff_sum = sum(coeffs)
    return [volume * coeff / coeff_sum for coeff in coeffs]


class StartPositions:
    def __init__(self, buy, sell, tick):
        self.buy = buy
        self.sell = sell
        self.tick = tick


class SQLEqualizerStrategy(metaclass=AbstractStrategy):
    def __init__(self, exchange, settings):
        self.exchange = exchange
        self.settings = settings
        self._instrument = None

        self.config = settings.SQL_EQUALIZER_CONF

        self.engine = sqlalchemy.create_engine(self.config['database_url'], pool_recycle=3600)

        self.sql_value_multiplier = self.config.get('sql_multiplier', 1)

        self.min_tick_diff = 1  # min diff between buy and sell
        self.tick_start = 0  # >0 - better than market; <0 - worse than market; 0 - like market
        self.bucket_count = 1  # number of orderds to split equalizing volume
        self.tick_step = 1  # tick step for prices for bucket splitting
        self.max_volume = None  # in XBT; `None` for no limit

        # in XBT - if `diff` in (-dt, dt) we stop equalizing
        self.diff_tolerance = self.config.get('tolerance', 1)
        self.distribute_function = lambda x: x + 1
        self.make_market_on_equlized = self.config.get('make_market_on_hedge', False)

        # volume of make market order will be `diff_tolerance * multiplier` (in XBT)
        self.make_market_tolerance_multiplier = self.config.get('make_market_multiplier', 2)


    def read_sql_value(self):
        with self.engine.begin() as connection:
            result = connection.execute(text("""
                SELECT SUM(MT4_NET_VOLUME) AS TOTAL_VOL_IN_BTC,
                       SUM(TOTAL_PROFIT) AS UNREALIZED_PL FROM
                (
                    SELECT SYMBOL, SUM(PROFIT) AS TOTAL_PROFIT,
                    CASE WHEN SYMBOL = 'BTCUSDmicro' THEN
                           ROUND(SUM(CASE CMD WHEN 1 THEN (VOLUME*-1)/100 ELSE VOLUME/100 END),2)
                      ELSE ROUND(SUM(CASE CMD WHEN 1 THEN (VOLUME*-1) ELSE VOLUME END),2)
                    END AS MT4_NET_VOLUME
                    FROM mt4_trades
                    WHERE CMD IN (0,1)
                    AND CLOSE_TIME = '1970-01-01 00:00:00'
                    AND SYMBOL IN('BTCUSD','BTCUSDmicro','BTCCNH','BTCEUR','BTCJPY')
                    GROUP BY SYMBOL
                ) t1
            """))
            exposure, unrealized_pl = result.fetchone()
            return float(exposure) * self.sql_value_multiplier, float(unrealized_pl)

    def get_start_positions(self):
        ticker = self.exchange.get_ticker()
        print(ticker)
        tick_size = self.instrument['tickSize']

        ticker_buy, ticker_sell = ticker['buy'], ticker['sell']

        logger.info('%s Ticker: Buy: %.2f, Sell: %.2f tickSize: %.2f',
                    self.instrument['symbol'], ticker_buy, ticker_sell,
                    tick_size)

        start = StartPositions(
            buy=ticker_buy + (self.tick_start * tick_size),
            sell=ticker_sell - (self.tick_start * tick_size),
            tick=tick_size
        )

        # check if we have orders in current ticker boundries
        # if so then we have to use them - otherwise we would be killing
        # spread in each step
        if ticker_buy == self.exchange.get_highest_buy()['price'] and start.buy > ticker_buy:
            start.buy = ticker_buy
        if ticker_sell == self.exchange.get_lowest_sell()['price'] and start.sell < ticker_sell:
            start.sell = ticker_sell

        # Back off if our spread is too small.
        tick_diff = (start.sell - start.buy) / tick_size
        if tick_diff < self.min_tick_diff:
            correction = math.ceil((self.min_tick_diff - tick_diff) / 2) * tick_size
            start.buy -= correction
            start.sell += correction
            logger.info('Spread to small - applying correction: %.2f', correction)

        logger.info('Start Positions: Buy: %.2f, Sell: %.2f', start.buy, start.sell)
        return start

    def get_target_order_book(self):
        # FIXME: convergence should not be sensitive to order in order book
        # Create orders from the outside in. This is intentional - let's say the inner order gets taken;
        # then we match orders from the outside in, ensuring the fewest number of orders are amended and only
        # a new order is created in the inside. If we did it inside-out, all orders would be amended
        # down and a new order would be created at the outside.
        current_position = self.exchange.calc_delta()['spot']
        sql_value = self.read_sql_value()
        position_diff = current_position - sql_value

        logger.info('Current position: %.2f MySQL value: %.2f Diff: %.2f',
                    current_position, sql_value, position_diff)

        if self.diff_tolerance is not None and abs(position_diff) <= self.diff_tolerance:
            if self.make_market_on_equlized:
                logger.info('Exposure equalized. Making market...')
                orders = self.make_market_orders(position_diff)
            else:
                logger.info('Exposure equalized. Idle...')
                orders = []
        else:
            if position_diff < 0:
                logger.info('Trying to equalize. Buying...')
            else:
                logger.info('Trying to equalize. Selling...')
            orders = self.equilize_orders(position_diff)

        return OrderBook(orders)

    def equilize_orders(self, diff):
        start = self.get_start_positions()

        volume = abs(diff)
        if self.max_volume is not None:
            volume = min(volume, self.max_volume)

        if diff < 0:
            side = Order.BUY
            start_price = start.buy
            sign = -1
        else:
            side = Order.SELL
            start_price = start.sell
            sign = 1

        volume_distribution = distribute(volume, self.bucket_count,
                                         self.distribute_function)

        for i in reversed(range(self.bucket_count)):
            order = self.prepare_order(
                side=side,
                start_price=start_price,
                value=volume_distribution[i],
                index=sign * i,
                tick_size=start.tick,
            )
            if order.orderQty > 0:
                yield order

    def make_market_orders(self, diff):
        start = self.get_start_positions()
        volume = self.diff_tolerance * self.make_market_tolerance_multiplier

        yield Order(
            side=Order.SELL,
            price=start.sell,
            orderQty=int(start.sell * volume),
        )
        yield Order(
            side=Order.BUY,
            price=start.buy,
            orderQty=int(start.buy * volume)
        )

    def prepare_order(self, side, start_price, value, index, tick_size):
        """Create an order object."""

        price = self.get_price_offset(start_price, index, tick_size)

        quantity = int(price * value)

        return Order(price=price, orderQty=quantity, side=side)

    def get_price_offset(self, start_position, index, tick_size):
        return round(start_position + (self.tick_step * index * tick_size),
                     self.instrument['tickLog'])
