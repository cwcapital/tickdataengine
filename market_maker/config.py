
# Each parameter has intended unit commented out to the right of the parameter value
# e.g. Max loss is specified in US dollars

# IMPORTANT
# XYZ contract is denominated as Bitmex contracts
# e.g. 5 XBTUSD
# CRYPTO units are denominated as units of a cryto-currency
# e.g. 5 BTC
# note that 5 BTC does not equal 5 XBTUSD
# Note that broker exposure is denoted in BTC, not XBTUSD contracts.
# Note that broker exposure is converted to BTC using the BitMEX mark price
# Note that client exposure as pulled from the MT4 report server is
#   assume to be MT4 client exposure denoted in BTC

# Net exposure is defined as the difference between broker exposure and
# client exposure
# NOTE: Net exposure is calculated as broker exchange exposure - client mt4 exposure
# To disable: set MAX_NET_EXPOSURE = 0
MAX_NET_EXPOSURE = 0 #XBTUSD contracts

# LET exposureConstant = ((Net_Exposure + New_Qty) / Available), where Net_Exposure
# is in XBT (not XBTUSD contracts), New_Qty is the quantity of the next limit order (
# also in XBT), and Available is the total amount of XBT available for trade on bitmex.
# THEN, if x >= MAX_EXPOSURE_RELATIVE_TO_EQUITY, then
# the bot will not place any buy limit orders.
# Vice-versa if x =< -MAX_EXPOSURE_RELATIVE_TO_EQUITY.
# e.g.- available equity on BitMEX = 7.5 XBT, net exposure = 20 XBT,
# BTCUSD price = 4000, MAX_EXPOSURE_RELATIVE_TO_EQUITY = 3 (i.e. - new orders
# cannot push net exposure past 3 times available equity on deposit).
# Suppose the bot receives signal to place: Buy limit 15000, Sell limit 2000.
# Then, since the buy limit order value = (1/4000) * 15000 = 3.75 XBT and
# exposureConstant = (20 + 3.75) / 7.5 = 3.1667 > 3, then the buy limit
# would not be placed. Conversely, the sell limit of 2000 would still be placed.
# NOTE: x is reduced as either Net Exposure is reduced,
# subsequent limits order's quantities are reduced,
# or as Available XBT increases
# NOTE: the feature will not be applied if CLIENT_EXPOSURE_DISCREPANCY_THRESHOLD
# is set, and the bot is attempting to reconcile to match client's exposure
MAX_EXPOSURE_RELATIVE_TO_EQUITY = 1.7 # Multiple of XBT avail on BitMEX

# The market maker bot will treat client exposure as MT4_exposure (from report server) * P.
# For example, if P = 0.5, and client expousre = 50.8 BTC,
# then client exposure will appear as 25.4 BTC to the bot.
# This will affect ALL calculations, even max loss calculations
CLIENT_EXPOSURE_TARGET_HEDGE_PERCENTAGE = 0.8 #PERCENT

# Defined as a percentage, taking a range of values between 0 and 1.
# If the difference (as measure in percentage) between broker exposure (in Bitmex)
# and client exposure (in MT4) is greater than the set threshold, then broker
# exposure will be fully reconciled to match client exposure.
# For example, suppose it was set to 50%. Assume that we have 20 BTC open in
# MT4 and 20 BTC open in BitMEX. If MT4 exposure then falls to 50% of
# BitMEX (aka 10 BTC), and we still have 20 BTC open in BitMEX,
# then it will trigger a reconciliation.
# ** Set less than zero to disable this feature
CLIENT_EXPOSURE_DISCREPANCY_THRESHOLD  = -1 # 0.4 PERCENT

# If max net exposure if crossed, net exposure will be reduced to the amount
# within X% of MAX_NET_EXPOSURE
MAX_NET_EXPOSURE_RECONCILE_PERC = 0.2 #PERCENT (of net exposure)

# Max allowable unrealized loss before a reconciliation is triggered
# To disable: set MAX_UNREALIZED_LOSS <= 0
MAX_UNREALIZED_LOSS = 10.0 # CRYTO-UNITS (BTC)

# If Max Loss is reached, net exposure will be reduced by the amount equal
# to X% of net exposure
MAX_LOSS_RECONCILE_PERC = 0.5 #PERCENT (of net exposure)

# Sets aggressiveness in placing buy orders: places a buy limit X USD below
# the current market bid
# NOTE: buySensitivity is simply any function that given a order book level
# to be placed in the orderbook returns an amount in USD (away from the
# current Bid)
def buySensitivity(orderBookLevel):
    return (orderBookLevel+0.1) #USD

# Given a buy level in the order book, returns a contract size
def buyOrderSize(orderBookLevel):
    return 10000 #XBTUSD contracts

# Number of buy levels to place in exchange orderbook
NO_OF_BUY_LEVELS = 1

# Sets aggressiveness in placing sell orders: places a sell limit X USD above
# the current market price
# NOTE: sellSensitivity is simply any function that given a order book level
# to be placed in the orderbook returns an amount in USD (away from the
# current Ask)
def sellSensitivity(orderBookLevel):
    return (orderBookLevel+0.1) #USD

# Given a sell level in the order book, returns a contract size
def sellOrderSize(orderBookLevel):
    return 10000 #XBTUSD contracts

# Number of sell levels to place in exchange orderbook
NO_OF_SELL_LEVELS = 1

# If the limit price (buy or sell) deviaiates by more than the LIMIT_PRICE_MAX_DEVIATION
# amount, then the order(s) -- all orders on the same side -- will be cancelled
# and replaced according to (buy/sell)Sensitivity
# NOTE: to Disable: set < 0
# NOTE: uses mid price between bid and ask
# NOTE: will be rounded multiple of DIGITS in _settings_base, e.g. - for XBTUSD,
# DIGITS = 1. So LIMIT_PRICE_MAX_DEVIATION = 0.55 would be rounded to 0.6
LIMIT_PRICE_MAX_DEVIATION = 0.5 # Quote currency (e.g. - XBTUSD)

# Rate to reduce client exposure as client's open new orders
# NOTE: if clients reduce exposure
CLIENT_EXPOSURE_REDUCTION_RATE = 0.2 #PERCENT

# Allows you to momentarily increase exposure against clients (to take advantage
# of market oscillations) while still reducing overall client exposure
COUNTER_CLIENT_EXPOSURE_RATE = 0.2 #PERCENT
