from abc import ABCMeta, abstractmethod


class AbstractStrategy(ABCMeta):
    @property
    def instrument(self):
        if self._instrument is None:
            self._instrument = self.exchange.get_instrument()
        return self._instrument

    @abstractmethod
    def get_target_order_book(self):
        pass
