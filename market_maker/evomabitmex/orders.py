class Order:
    BUY = 'Buy'
    SELL = 'Sell'

    def __init__(self, price, orderQty, side, post_only=True):
        self.price = price
        self.orderQty = orderQty
        self.side = side
        self.post_only = post_only

    def to_dict(self):
        order = {
            'price': self.price,
            'orderQty': self.orderQty,
            'side': self.side,
        }
        if self.post_only:
            order['execInst'] = 'ParticipateDoNotInitiate'
        return order


class OrderBook:
    def __init__(self, orders):
        self.buy_orders = []
        self.sell_orders = []
        for order in orders:
            if order.side == Order.BUY:
                self.buy_orders.append(order)
            elif order.side == Order.SELL:
                self.sell_orders.append(order)
            else:
                raise ValueError('Bad order side: {}'.format(order.side))
