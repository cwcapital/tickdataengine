from market_maker.settings import settings
from market_maker import config as c
from datetime import datetime as dt
import os
import threading
import requests

counter = 1
TICK_DATABASE_URL = ''

with open('config.txt', 'r') as f:
    TICK_DATABASE_URL = f.readline().rstrip('\n').strip(' ')

def post_to_server(data):
    bEmpty = True

    try:
        bEmpty = os.stat('temp.txt').st_size == 0
        if not bEmpty:
            with open('temp.txt', 'r') as f:
                data = f.read() + data
    except FileNotFoundError:
        pass

    if len(data) > 0:
        try:
            res = requests.post(TICK_DATABASE_URL, {
                'exchange': 'bitmex',
                'symbol': 'xbtusd',
                'data': data
            })
        except requests.exceptions.ConnectionError:
            res = requests.Request()
            res.text = '{"status": "fail", "message": "server unavailable"}',
            res.status_code = 500

        if res.status_code == 200:
            if not bEmpty:
                os.unlink('temp.txt')
        else:
            with open('temp.txt', 'w') as f:
                f.write(data)


class TickBin():
    ticks = ''
    last_tick = (0,0)

    @classmethod
    def insert(cls, bid, ask):
        if (bid,ask) != cls.last_tick:
            cls.ticks += '{0},{1},{2};'.format(
                dt.now().strftime('%Y-%m-%d %H:%M:%S'),
                bid,
                ask)
            cls.last_tick = (bid,ask)

def init(manager):
    print('main loop initialization complete!')

def main(manager):
    global counter

    ticker = manager.exchange.get_ticker()
    TickBin.insert(ticker['buy'], ticker['sell'])

    if counter % 20 == 0:
        t = threading.Thread(target=post_to_server, args=(TickBin.ticks,))
        t.start()
        TickBin.ticks = ''

    counter += 1
